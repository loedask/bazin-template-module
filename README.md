# Bazin Template

## English

### Description

BazinTemplate is a Laravel module created by Daskana. It is designed to provide developers with a set of pre-designed CSS styles, JavaScript components, and HTML templates. These resources are intended to assist in the creation of visually appealing and consistent user interfaces within Yindula projects.

The term 'Bazin' refers to a traditional outerwear garment primarily worn by men and women in West Africa. In this context, the creator envisioned this module as a stylish Laravel component that not only adorns but also enhances the design of his applications.

By incorporating this module, he aims to provide a visually appealing and aesthetically pleasing appearance to his Laravel projects, elevating their overall presentation.

### Prerequisites

- [Node.js](https://nodejs.org) installed.
- [Laravel Framework](https://laravel.com) installed.
- [Laravel Modules](https://github.com/nwidart/laravel-modules) installed.
- [Laravel Module Installer](https://github.com/joshbrw/laravel-module-installer) installed.

### Important Notice

1. Before proceeding, ensure that you have installed the "[nwidart/laravel-modules](https://github.com/nwidart/laravel-modules)" package in your Laravel project.
Follow the provided instructions to install this package, as it allows for module installation within Laravel projects.

2. Once the "nwidart/laravel-modules" package is installed, an additional step is required to install modules into the "Modules" directory of your project.

3. To facilitate the automatic movement of Yindula CMS module files, you need to install the "[joshbrw/laravel-module-installer](https://github.com/joshbrw/laravel-module-installer)" composer plugin.

To install the plugin, open a terminal in the root directory of your Laravel project and execute the following command:

   ```bash
    composer require joshbrw/laravel-module-installer
   ```

4. After successfully installing the "joshbrw/laravel-module-installer" plugin, you can proceed with the installation of the Yindula CMS module.
During the installation process, the Yindula CMS module will be automatically placed in the "Modules" directory of your project.

Refer to the "Installation and Build" section for the appropriate command or instructions to install a specific module.
The "joshbrw/laravel-module-installer" plugin will handle the automatic movement of Yindula CMS module files to the "Modules" directory.

### Installation and Build

To install Bazin Template in your application, follow these steps:

1. Open a terminal in the root directory of your application.

2. Use the following command to install Bazin Template:

   ```bash
    composer require loecoscorp/bazin-template-module
   ```

3. Navigate to the `Modules/BazinTemplate` directory using the following command:

   ```bash
   cd Modules/BazinTemplate
   ```

4. Install the necessary Node.js dependencies by running the following command:

   ```bash
   npm install
   ```

5. Build the frontend assets by executing the following command:

   ```bash
   npm run build
   ```

6. Finally, compile and build the assets for development by running the following command:

   ```bash
   IF "%NODE_ENV%"=="production" (npm run prod) ELSE (npm run dev)
   ```

## Français

### Description

BazinTemplate est un module Laravel créé par Daskana. Il est conçu pour fournir aux développeurs un ensemble de styles CSS pré-conçus, de composants JavaScript et de modèles HTML. Ces ressources sont destinées à aider à la création d'interfaces utilisateur visuellement attrayantes et cohérentes dans les projets Yindula.

Le terme "Bazin" fait référence à un vêtement traditionnel porté principalement par les hommes et les femmes en Afrique de l'Ouest. Dans ce contexte, le créateur a imaginé ce module comme un composant Laravel élégant qui non seulement orne, mais aussi améliore la conception de ses applications.

En incorporant ce module, il vise à offrir une apparence visuellement attrayante et esthétiquement plaisante à ses projets Laravel, rehaussant ainsi leur présentation globale.

### Prérequis

- [Node.js](https://nodejs.org) installé.
- Framework [Laravel](https://laravel.com) installé.
- Module [Laravel Modules](https://docs.laravelmodules.com/v10/introduction) installé.

### Installation et construction

Pour installer le module Bazin Template dans votre application, suivez ces étapes :

1. Ouvrez un terminal dans le répertoire principal de votre application.

2. Utilisez la commande suivante pour installer Bazin Template :

   ```bash
    composer require loecoscorp/bazin-template-module
   ```

3. Naviguez vers le répertoire `Modules/BazinTemplate` en utilisant la commande suivante :

   ```bash
   cd Modules/BazinTemplate
   ```

4. Installez les dépendances nécessaires de Node.js en exécutant la commande suivante :

   ```bash
   npm install
   ```

5. Construisez les ressources frontales en exécutant la commande suivante :

   ```bash
   npm run build
   ```

6. Enfin, compilez et construisez les ressources pour le développement en exécutant la commande suivante :

   ```bash
   IF "%NODE_ENV%"=="production" (npm run prod) ELSE (npm run dev)
   ```
