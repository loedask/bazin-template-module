document.addEventListener('DOMContentLoaded', function() {
    // script to activate parent menu if sub menu has currently active
    var hasActiveMenu = document.querySelectorAll('.nav-item.dropdown ul li.active').length > 0;
    if (hasActiveMenu) {
      var activeSubMenu = document.querySelector('.nav-item.dropdown ul li.active');
      activeSubMenu.parentNode.style.display = 'block';
      activeSubMenu.parentNode.parentNode.classList.add('active');
    }
  });
