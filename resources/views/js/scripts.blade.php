@php
    if (class_exists(Modules\FrontPages\Utilities\HandleUrlPrefix::class)) {
        $path_prefix = (new Modules\FrontPages\Utilities\HandleUrlPrefix())->handle();
        $modules_folder_name = (new Modules\FrontPages\Utilities\HandleUrlPrefix())->getModulesFolderName();
    }
@endphp


@if (empty($path_prefix))
    <!-- General JS Scripts -->
    <script src="{{ asset('build-bazintemplate/js/jquery.min.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/popper.min.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/iziToast.min.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/select2.min.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/jquery.nicescroll.js') }}"></script>


    <!-- Template JS File -->
    <script src="{{ asset('build-bazintemplate/js/scripts.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/stisla.js') }}"></script>
    <script src="{{ asset('build-bazintemplate/js/authed_menu.js') }}"></script>

@else
    <!-- General JS Scripts -->
    <script src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/modules/jquery.min.js') }}"></script>
    <script src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/modules/popper.js') }}"></script>

    <script src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/modules/bootstrap/js/bootstrap.min.js') }}">
    </script>

    <script
        src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/modules/nicescroll/jquery.nicescroll.min.js') }}">
    </script>

    <script src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/js/particlesjs/particles.js') }}"></script>

    <script src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/js/particlesjs/bazin.js') }}"></script>

    <!-- Template JS File -->
    <script src="{{ asset($path_prefix . $modules_folder_name . 'bazintemplate/js/stisla.js') }}"></script>
@endif
