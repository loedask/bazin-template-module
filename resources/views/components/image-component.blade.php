{!! Form::label('image', $alt . ':') !!}
<p>
    <img src="{{ $src }}" alt="Slider Image" class="{{ $alt }}" style="max-width: 100%; height: auto;">
</p>
