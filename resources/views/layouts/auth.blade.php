<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@yield('title') | {{ config('app.name') }}</title>

    {{ module_vite('build-bazintemplate', 'resources/assets/sass/auth.scss') }}

    @yield('page_css')
    @yield('css')


</head>

<body>


    <div id="app">

        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div
                        class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">

                        @yield('content')

                    </div>
                </div>
            </div>
        </section>
    </div>

</body>

{{ module_vite('build-bazintemplate', 'resources/assets/js/auth.js') }}

<!-- Page Specific JS File -->
@stack('page_js')
@stack('scripts')


</html>
