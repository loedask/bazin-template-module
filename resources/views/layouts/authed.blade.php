<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <x-yindulacore::plugins.livewire-styles />

    {{ module_vite('build-bazintemplate', 'resources/assets/sass/app.scss') }}

    @yield('page_css')
    @yield('css')

    {{ module_vite('build', 'resources/js/app.js') }}

</head>

<body>

    <div id="app">
        <div class="main-wrapper main-wrapper-1">
            <div class="navbar-bg"></div>

            <nav class="navbar navbar-expand-lg main-navbar">
                @include('bazintemplate::common.header')
            </nav>

            <div class="main-sidebar main-sidebar-postion">
                @if (Module::find('Core'))
                    @include('core::partials.sidebar')
                @else
                    @include('bazintemplate::common.sidebar')
                @endif
            </div>

            <!-- Main Content -->
            <div class="main-content">

                @yield('breadcrumbs')

                @if (Module::find('YindulaCore'))
                    <x-yindulacore::impersonation-alert />
                @endif

                @yield('content')
            </div>

            <footer class="main-footer">
                @include('bazintemplate::common.footer')
            </footer>
        </div>
    </div>

    @include('bazintemplate::profile.change_password')
    @include('bazintemplate::profile.edit_profile')


    <x-yindulacore::plugins.livewire-scripts />
    {{-- <x-yindulacore::plugins.livewire-sortable-js />
     <x-yindulacore::plugins.alpinejs /> --}}

    <!-- Page Specific JS File -->
    @stack('page_js')
    @stack('scripts')


    @include('bazintemplate::js.scripts')


</body>

{{-- --}}


</html>
