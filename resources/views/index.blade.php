@extends('bazintemplate::layouts.authed')

@section('content')
    <h1>{!! config('bazintemplate.name') !!}</h1>

    <p>
        This view is loaded from module: {!! config('bazintemplate.name') !!}
    </p>
@endsection
