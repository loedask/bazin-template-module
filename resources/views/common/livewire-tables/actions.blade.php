<div class='btn-group'>
    <a href="{{ $showUrl }}" class='btn btn-primary btn-xs'>
        <i class="fa fa-list"></i>
    </a>
    <a href="{{ $editUrl }}" class='btn btn-secondary btn-xs'>
        <i class="fa fa-edit"></i>
    </a>
    <a class='btn btn-danger btn-xs' wire:click="deleteRecord({{ $recordId }})"
       onclick="confirm('Are you sure you want to remove this Record?') || event.stopImmediatePropagation()">
        <i class="fa fa-trash"></i>
    </a>
</div>
