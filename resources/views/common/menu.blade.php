<li class="side-menus {{ Request::is('yindula/dashboard*') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('yindula.dashboard') }}">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

@if (Module::find('YindulaCv'))
    @if (Module::find('YindulaCv')->isEnabled())
        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-users-cog"></i>
                <span>
                    {{ __('CV') }}
                </span>
            </a>
            <ul class="dropdown-menu">
                @include('yindulacv::partials.menu')
            </ul>
        </li>
    @endif
@endif

@if (Module::find('YindulaCms'))
    @if (Module::find('YindulaCms')->isEnabled())

        <li class="nav-item dropdown">
            <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-users-cog"></i>
                <span>
                    {{ __('CMS') }}
                </span>
            </a>
            <ul class="dropdown-menu">
                @include('yindulacms::partials.menu')
            </ul>
        </li>
    @endif
@endif


@if (Module::find('Mbongo'))
    @if (Module::find('Mbongo')->isEnabled())
        <li class="nav-item dropdown">

            <a href="#" class="nav-link has-dropdown">
                <i class="fas fa-money-bill"></i>
                <span>
                    {{ __('Budget Planner') }}
                </span>
            </a>
            <ul class="dropdown-menu">
                @include('mbongo::partials.menu')
            </ul>
        </li>
    @endif
@endif
