@push('page_scripts')
    <script src="https://unpkg.com/filepond@4.9.5/dist/filepond.js"></script>
    <script>
        // Set default FilePond options
        FilePond.setOptions({
            server: {
                url: "{{ config('filepond.server.url') }}",
                headers: {
                    'X-CSRF-TOKEN': "{{ @csrf_token() }}",
                }
            }
        });

        // Create the FilePond instance
        FilePond.create(document.querySelector('input[name="image"]'));
        // FilePond.create(document.querySelector('input[name="filename[]"]'));
    </script>
@endpush
