<aside id="sidebar-wrapper">
    <div class="sidebar-brand">
        <img class="navbar-brand-full app-header-logo" src="{{ asset('build-bazintemplate/images/globe_africa.jpg') }}" alt="Yindula Logo">
        <a href="{{ url('/') }}"></a>
    </div>
    <div class="sidebar-brand sidebar-brand-sm">
        <a href="{{ url('/') }}" class="small-sidebar-text">
            <img class="navbar-brand-full" src="{{ asset('build-bazintemplate/images/globe_africa.jpg') }}"  alt="" />
        </a>
    </div>
    <ul class="sidebar-menu">
        @include('bazintemplate::common.menu')
    </ul>
</aside>
