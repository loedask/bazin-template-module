<?php

namespace Modules\BazinTemplate\Utilities;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Config;
use Modules\YindulaCore\Utilities\Application as UtilitiesApplication;

class Application
{
    /**
     * Get the name of the application.
     *
     * @return string
     */
    public function name()
    {
        return Config::get('bazintemplate.name');
    }

}
