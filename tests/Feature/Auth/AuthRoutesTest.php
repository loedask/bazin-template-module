<?php

namespace Modules\BazinTemplate\Tests\Feature\Auth;

use Tests\TestCase;
use App\Models\User;

class AuthRoutesTest extends TestCase
{
    /**
     * Test if the login page is accessible.
     *
     * @return void
     */
    public function testLoginPage()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    /**
     * Test if the registration page is accessible.
     *
     * @return void
     */
    public function testRegistrationPage()
    {
        $response = $this->get('/register');

        $response->assertStatus(200);
    }

    /**
     * Test if the password reset page is accessible.
     *
     * @return void
     */
    public function testPasswordResetPage()
    {
        $response = $this->get('/password/reset');

        $response->assertStatus(200);
    }

    /**
     * Test if the email verification notice page is accessible.
     *
     * @return void
     */
    public function testEmailVerificationNoticePage()
    {
        $response = $this->get('/email/verify');

        $response->assertStatus(200);
    }

    /**
     * Test if the email verification page is accessible.
     *
     * @return void
     */
    public function testEmailVerificationPage()
    {
        $url = '/email/verify/' . $this->createTestUser()->id . '/' . sha1($this->createTestUser()->email);

        $response = $this->get($url);

        $response->assertStatus(200);
    }

    /**
     * Create a test user for email verification test.
     *
     * @return \App\Models\User
     */
    private function createTestUser()
    {
        return User::factory()->create([
            'email_verified_at' => null,
        ]);
    }
}
