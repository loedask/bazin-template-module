<?php

return [
    'name' => 'Bazin Template',

    /*
    |--------------------------------------------------------------------------
    | Application Debug Mode
    |--------------------------------------------------------------------------
    |
    | When your application is in debug mode, detailed error messages with
    | stack traces will be shown on every error that occurs within your
    | application. If disabled, a simple generic error page is shown.
    |
    */

    'bazin_fortify' => (bool) env('BAZIN_FORTIFY', false)
];
