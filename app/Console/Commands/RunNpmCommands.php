<?php

namespace Modules\BazinTemplate\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class RunNpmCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bazin:run-all-npm';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run npm install, npm run build, and npm run dev';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Running npm install...');
        $this->runNpmCommand('install');

        // Add npm i vite-plugin-static-copy
        $this->info('Running npm i vite-plugin-static-copy (See: https://www.npmjs.com/package/vite-plugin-static-copy)...');
        $this->runNpmCommand('i vite-plugin-static-copy');

        $this->info('Running npm run build...');
        $this->runNpmCommand('run build');

        // Add import statement for vite-plugin-static-copy to vite.config.js
        // $this->addImportStatementToViteConfig();

        $this->info('Running npm run dev...');
        $this->runNpmCommand('run dev');

        $this->info('Npm commands executed successfully.');
    }


    /**
     * Run npm command.
     *
     * @param string $command
     */
    private function runNpmCommand($command)
    {
        $modulePath = base_path('Modules/BazinTemplate');
        exec("cd $modulePath && npm $command");
    }


    /**
     * Add import statement for vite-plugin-static-copy to vite.config.js
     */
    private function addImportStatementToViteConfig()
    {
        $configFilePath = base_path('Modules/BazinTemplate/vite.config.js');

        // Check if the file exists
        if (File::exists($configFilePath)) {
            // Read the content of the file
            $content = File::get($configFilePath);

            // Add the import statement if not already present
            if (strpos($content, 'import { viteStaticCopy } from \'vite-plugin-static-copy\';') === false) {
                // Find the last import statement
                $lastImportPosition = strrpos($content, 'import');
                $content = substr_replace($content, "import { viteStaticCopy } from 'vite-plugin-static-copy';\n", $lastImportPosition, 0);

                // Write the modified content back to the file
                File::put($configFilePath, $content);

                $this->info('Import statement for vite-plugin-static-copy added to vite.config.js.');
            } else {
                $this->info('Import statement for vite-plugin-static-copy already exists in vite.config.js.');
            }
        } else {
            $this->error('vite.config.js file not found. Make sure the file exists.');
        }
    }
}
