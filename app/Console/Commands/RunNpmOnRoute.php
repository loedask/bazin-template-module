<?php

namespace Modules\BazinTemplate\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;

class RunNpmOnRoute extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bazin:run-npm-on-route';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run npm install, npm run build, and npm run dev';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $this->info('Running npm install...');
        $this->runNpmCommand('install');

        $this->info('Running npm run build...');
        $this->runNpmCommand('run build');

        $this->info('Running npm run dev...');
        $this->runNpmCommand('run dev');

        $this->info('Npm commands executed successfully.');
    }

    /**
     * Run npm command.
     *
     * @param string $command
     */
    private function runNpmCommand($command)
    {
        exec("npm $command");
    }
}
