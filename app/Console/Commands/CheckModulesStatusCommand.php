<?php

namespace Modules\BazinTemplate\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\File;

class CheckModulesStatusCommand extends Command
{
    protected $signature = 'bazin:check-modules-status';

    protected $description = 'Check and update modules_statuses.json file';

    public function handle()
    {
        $filePath = storage_path('modules_statuses.json');

        if (!File::exists($filePath)) {
            // If the file doesn't exist, create it with "BazinTemplate": true
            File::put($filePath, json_encode(['BazinTemplate' => true], JSON_PRETTY_PRINT));
            $this->info('modules_statuses.json created with "BazinTemplate": true');
        } else {
            // If the file exists, read its contents
            $contents = json_decode(File::get($filePath), true);

            // Check if "BazinTemplate" key exists, if not, add it with a value of true
            if (!isset($contents['BazinTemplate'])) {
                $contents['BazinTemplate'] = true;
                File::put($filePath, json_encode($contents, JSON_PRETTY_PRINT));
                $this->info('"BazinTemplate": true added to modules_statuses.json');
            } else {
                // Check if "BazinTemplate" is either true or false, if not, set it to true
                if ($contents['BazinTemplate'] !== true && $contents['BazinTemplate'] !== false) {
                    $contents['BazinTemplate'] = true;
                    File::put($filePath, json_encode($contents, JSON_PRETTY_PRINT));
                    $this->info('"BazinTemplate" set to true in modules_statuses.json');
                } else {
                    $this->info('"BazinTemplate" already exists in modules_statuses.json');
                }
            }
        }
    }
}
