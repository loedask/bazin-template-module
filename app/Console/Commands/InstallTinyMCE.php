<?php

namespace Modules\BazinTemplate\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;

class InstallTinyMCE extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bazin:install-tinymce {module?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install TinyMCE using npm';

    /**
     * Execute the console command.
     */
    public function handle()
    {
        // Set the module path
        $module = $this->argument('module');
        $module_path = $module ? base_path("Modules/$module") : base_path();

        // Change the current working directory to the module path
        chdir($module_path);

        // Run npm install for TinyMCE
        $this->info('Installing TinyMCE in ' . $module_path . '...');
        $this->runNpmCommand('npm install tinymce');

        // Configure app.js
        $this->configureAppJs($module_path);

        $this->runNpmCommand('npm run build');

        $this->info('TinyMCE installed successfully!');
    }

    /**
     * Run npm command.
     *
     * @param string $command
     */
    private function runNpmCommand($command)
    {
        $this->line('Running: ' . $command);

        $process = proc_open($command, [0 => STDIN, 1 => STDOUT, 2 => STDERR], $pipes);
        if ($process !== false) {
            proc_close($process);
        }
    }



    /**
     * Configure app.js with TinyMCE imports and initialization.
     *
     * @param string $modulePath
     */
    private function configureAppJs($modulePath)
    {
        $file_system = new Filesystem();
        $js_path = $modulePath . '/resources/js/app.js';

        if ($file_system->exists($js_path)) {
            // Add TinyMCE imports and initialization to app.js
            $content = $file_system->get($js_path);
            $content .= "\n\n" . $this->getTinyMCEConfig();
            $file_system->put($js_path, $content);
        } else {
            $this->error('app.js not found. Please make sure the file exists.');
        }
    }

    /**
     * Get TinyMCE configuration for app.js.
     *
     * @return string
     */
    private function getTinyMCEConfig()
    {
        return <<<'JS'
import "tinymce/tinymce";
import "tinymce/skins/ui/oxide/skin.min.css";
import "tinymce/skins/content/default/content.min.css";
import "tinymce/skins/content/default/content.css";
import "tinymce/icons/default/icons";
import "tinymce/themes/silver/theme";
import "tinymce/models/dom/model";

// Init TinyMCE ..
window.addEventListener("DOMContentLoaded", () => {
    tinymce.init({
        selector: "textarea",
        /* TinyMCE configuration options */
        skin: false,
        content_css: false,
    });
});
JS;
    }
}
