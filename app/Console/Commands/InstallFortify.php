<?php

namespace Modules\BazinTemplate\app\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\File;


class InstallFortify extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bazin:install-fortify';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install and publish laravel/fortify package';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // Display a message indicating the installation process has started
        $this->info('Installing laravel/fortify...');

        // Execute Composer command to require the package
        exec('composer require laravel/fortify');

        // Display a success message once the package is installed
        $this->info('laravel/fortify installed successfully!');

        // Publish the Fortify assets
        $this->info('Publishing Fortify assets...');
        exec('php artisan vendor:publish --provider="Laravel\Fortify\FortifyServiceProvider"');

        // Display a success message once the assets are published
        $this->info('Fortify assets published successfully!');

        // Run the migrations
        $this->info('Running Fortify migrations...');
        exec('php artisan migrate');

        // Display a success message once the migrations are complete
        $this->info('Fortify migrations completed successfully!');

        // Update fortify.php configuration
        $this->updateFortifyConfig();

        // Add FortifyServiceProvider to config/app.php
        $this->addFortifyServiceProviderToConfig();

        // Display a final success message
        $this->info('Fortify configuration updated successfully!');
    }

    /**
     * Update fortify.php configuration file.
     *
     * @return void
     */
    protected function updateFortifyConfig()
    {
        $configPath = config_path('fortify.php');

        // Check if the fortify.php config file exists
        if (File::exists($configPath)) {
            // Read the current content of fortify.php
            $configContent = File::get($configPath);

            // Replace RouteServiceProvider::HOME with Modules\YindulaCore\Enums\HomeEnum::YINDULA_DASHBOARD
            $updatedContent = str_replace(
                'RouteServiceProvider::HOME',
                'Modules\YindulaCore\Enums\HomeEnum::YINDULA_DASHBOARD',
                $configContent
            );

            // Write the updated content back to fortify.php
            File::put($configPath, $updatedContent);
        } else {
            // Display a warning if the config file doesn't exist
            $this->warn('fortify.php configuration file not found.');
        }
    }

    /**
     * Add FortifyServiceProvider to the providers array in config/app.php.
     *
     * @return void
     */
    protected function addFortifyServiceProviderToConfig()
    {
        $configPath = config_path('app.php');

        // Check if the app.php config file exists
    if (File::exists($configPath)) {
        // Read the current content of app.php
        $configContent = File::get($configPath);

        // Check if FortifyServiceProvider is already in the providers array
        if (strpos($configContent, 'App\Providers\FortifyServiceProvider::class') === false) {
            // Find the position to insert FortifyServiceProvider in the providers array
            $providerArrayStart = strpos($configContent, "'providers' => [") + 16;

            // Use ServiceProvider::defaultProviders() as a reference point
            $referencePoint = 'ServiceProvider::defaultProviders()->merge([';
            $referencePosition = strpos($configContent, $referencePoint, $providerArrayStart);

            // Find the end of the default providers section
            $endOfDefaultProviders = strpos($configContent, ']', $referencePosition);

            // Prepare the line to be added
            $lineToAdd = str_repeat(' ', $providerArrayStart + 4) . "App\Providers\FortifyServiceProvider::class,";

            // Insert FortifyServiceProvider after the end of default providers
            $updatedContent = substr_replace($configContent, $lineToAdd . PHP_EOL, $endOfDefaultProviders - 1, 0);

            // Write the updated content back to app.php
            File::put($configPath, $updatedContent);

            // Display a success message
            $this->info('FortifyServiceProvider added to config/app.php successfully!');
        } else {
            // Display a warning if the provider is already in the array
            $this->warn('FortifyServiceProvider is already in config/app.php.');
        }
    } else {
        // Display a warning if the config file doesn't exist
        $this->warn('config/app.php configuration file not found.');
    }
    }
}
