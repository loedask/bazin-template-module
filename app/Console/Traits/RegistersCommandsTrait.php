<?php

namespace Modules\BazinTemplate\app\Console\Traits;

use Modules\BazinTemplate\app\Console\Commands\RunNpmOnRoute;
use Modules\BazinTemplate\app\Console\Commands\InstallFortify;
use Modules\BazinTemplate\app\Console\Commands\InstallTinyMCE;
use Modules\BazinTemplate\app\Console\Commands\RunNpmCommands;
use Modules\BazinTemplate\app\Console\RunBazinTemplateSetupCommands;
use Modules\BazinTemplate\app\Console\Commands\CheckModulesStatusCommand;


trait RegistersCommandsTrait
{
    /**
     * Register commands in the format of Command::class
     */
    protected function registerCommands(): void
    {
        $this->commands([
            RunBazinTemplateSetupCommands::class,
            InstallFortify::class,
            RunNpmCommands::class,
            RunNpmOnRoute::class,
            CheckModulesStatusCommand::class,
            InstallTinyMCE::class,
        ]);
    }
}
