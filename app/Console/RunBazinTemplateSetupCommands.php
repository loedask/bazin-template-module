<?php

namespace Modules\BazinTemplate\app\Console;

use Illuminate\Console\Command;

class RunBazinTemplateSetupCommands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'bazin:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run setup commands: migrate, seed, passport:install, passport:keys';

    /**
     * Execute the console command.
     */
    public function handle()
    {
         // Run the 'php artisan module:make-and-check' command
        $this->call('bazin:run-all-npm');

         // Run the 'php artisan module:make-and-check' command
        $this->call('bazin:run-npm-on-route');
    }

}

