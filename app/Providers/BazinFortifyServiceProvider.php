<?php

namespace Modules\BazinTemplate\app\Providers;

use Illuminate\Support\Facades\Config;
use Illuminate\Support\ServiceProvider;
use Laravel\Fortify\Fortify;

class BazinFortifyServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (class_exists(Fortify::class)) {

            Fortify::loginView(function () {
                return view('bazintemplate::auth.login');
            });

            Fortify::registerView(function () {
                return view('bazintemplate::auth.register');
            });

            // Fortify::confirmPasswordView(function () {
            //     return view('bazintemplate::auth.passwords.confirm');
            // });

            // Fortify::requestPasswordResetLinkView(function () {
            //     return view('bazintemplate::auth.passwords.email');
            // });

            // Fortify::resetPasswordView(function () {
            //     return view('bazintemplate::auth.passwords.reset');
            // });

            // Fortify::verifyEmailView(function () {
            //     return view('bazintemplate::auth.verify');
            // });

        }
    }
}
