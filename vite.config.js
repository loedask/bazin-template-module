import { defineConfig } from 'vite';
import { viteStaticCopy } from 'vite-plugin-static-copy';
import path from "path";
import laravel from 'laravel-vite-plugin';
import FullReload from 'vite-plugin-full-reload';

export default defineConfig({
    build: {
        outDir: '../../public/build-bazintemplate',
        emptyOutDir: true,
        manifest: true,
    },
    plugins: [
        laravel({
            publicDirectory: '../../public',
            buildDirectory: 'build-bazintemplate',
            input: [
                __dirname + '/resources/assets/sass/app.scss',
                __dirname + '/resources/assets/js/app.js',

                __dirname + "/resources/assets/sass/base_frontend.scss",
                __dirname + "/resources/assets/sass/frontend.scss",
                __dirname + "/resources/assets/js/frontend.js",

                __dirname + "/resources/assets/sass/auth.scss",
                __dirname + "/resources/assets/js/auth.js",

                 __dirname + "/resources/assets/js/scripts.js",
                 __dirname + "/resources/assets/js/stisla.js",
                 __dirname + "/resources/assets/js/authed_menu.js",

                 __dirname + "/resources/assets/js/custom.js"
            ],
            refresh: true,
        }),
        {
           name: 'blade',
           handleHotUpdate({ file, server }) {
               if (file.endsWith('.blade.php')) {
                   server.ws.send({
                       type: 'full-reload',
                       path: '*',
                   });
               }
           },
       },
        FullReload(['config/routes.rb', 'app/views/**/*']),
        viteStaticCopy({
          targets: [
            {
              src: "resources/assets/modules/jquery.min.js",
              dest: "js",
            },
            {
              src: "resources/assets/modules/popper.min.js",
              dest: "js",
            },
            {
              src: "resources/assets/modules/bootstrap/js/bootstrap.min.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/iziToast.min.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/select2.min.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/jquery.nicescroll.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/scripts.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/stisla.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/auth.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/authed_menu.js",
              dest: "js",
            },

            {
              src: "resources/assets/js/particlesjs/particles.js",
              dest: "js",
            },
            {
              src: "resources/assets/js/particlesjs/bazin.js",
              dest: "js",
            },

            {
              src: "resources/assets/images/globe_africa.jpg",
              dest: "images",
            },
            {
              src: "resources/assets/images/no-image.png",
              dest: "images",
            },
          ],
        }),
    ],
    resolve: {
      alias: {
        jquery: path.resolve(__dirname, "node_modules/jquery"),
        "@popperjs": path.resolve(__dirname, "node_modules/@popperjs"),
        "tooltip.js": path.resolve(__dirname, "node_modules/tooltip.js"),
        bootstrap: path.resolve(__dirname, "node_modules/bootstrap"),
        "jquery.nicescroll": path.resolve(
          __dirname,
          "node_modules/jquery.nicescroll"
        ),
        izitoast: path.resolve(__dirname, "node_modules/izitoast"),
        select2: path.resolve(__dirname, "node_modules/select2"),
        moment: path.resolve(__dirname, "node_modules/moment"),
        "particles.js": path.resolve(__dirname, "node_modules/particles.js"),
      },
    },
});

export const paths = [
   'Modules/$STUDLY_NAME$/resources/assets/sass/app.scss',
   'Modules/$STUDLY_NAME$/resources/assets/js/app.js',
];
